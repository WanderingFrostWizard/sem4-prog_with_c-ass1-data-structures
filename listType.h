/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  list.h - Implementation of the std::list class                           #
 *#                                                                           #
 *###########################################################################*/

#ifndef LISTTYPE_H
#define LISTTYPE_H

#include <list>
#include <iostream>

class ListType
{
	std::list<std::string> list;

	int wordcount;
	public: 
		ListType( void )  {}
	bool add(std::string);
    bool search(std::string);
    void print(void);
};

#endif