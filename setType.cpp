/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  setType.cpp - Implementation of the std::multiset class                  #
 *#                                                                           #
 *###########################################################################*/

#include "setType.h"


bool SetType::add(std::string word)
{
    list.insert( word );
    ++wordcount;

    return true;
}

// Searches for the first instance of the target word
bool SetType::search(std::string targetWord)
{
    // If the word is NOT found, list.find() will return list.end()
    if ( list.find( targetWord ) == list.end() )
    {
        return false;
    }    
    return true;
}

void SetType::print(void)
{
    for (std::multiset<std::string>::iterator it = list.begin(); 
            it!=list.end(); ++it )
        std::cout << *it << std::endl;
}