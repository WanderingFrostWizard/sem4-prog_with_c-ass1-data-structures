/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  checker.h - variables and function prototypes used in checker.cpp        #
 *#                                                                           #
 *###########################################################################*/

#ifndef CHECKER_H
#define CHECKER_H

#include <algorithm>        // For string transform function (to lowercase)
#include <string>           // For string functions
#include <iostream>         // For cin and cout
#include <cstdlib>
#include <streambuf>
#include <fstream>          // For file reading
#include <map>              // For std::map

#include <boost/foreach.hpp>    // To allow access to the boost foreach statement
#include <boost/tokenizer.hpp>  // To allow tokenizing 

#include "listType.h"       // Include the std::list data type
#include "vectorType.h"     // Include the std::vector data type
#include "setType.h"        // Include the std::multiset data type
#include "custom_List.h"    // Include the custom_list data type
#include "custom_Tree.h"    // Include the custom_tree data type

class Checker
{
    class wordcounter
    {
        std::map<std::string, int> countMap;

        public: 
            wordcounter( void )  {}
        bool add(std::string);
        // bool search(std::string);
        void print(void);
        void printToFile( std::string );

        friend class Checker;
    };
        
    public:
    wordcounter counter;

    bool setStructure(std::string structure, std::string dictionary, std::string textfile);
};

#endif