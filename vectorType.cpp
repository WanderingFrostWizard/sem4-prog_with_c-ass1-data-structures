/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  vectorType.cpp - Implementation of the std::vector class                 #
 *#                                                                           #
 *###########################################################################*/

#include "vectorType.h"


bool VectorType::add(std::string word)
{
	list.push_back( word );
	++wordcount;

    return true;
}

// Searches for the first instance of the target word
bool VectorType::search(std::string targetWord)
{
	for ( std::vector<std::string>::iterator it = list.begin() ; it != list.end(); ++it )
	{
		if ( *it == targetWord )
			return true;		
	}
    return false;
}

void VectorType::print(void)
{
	for ( std::vector<std::string>::iterator it = list.begin() ; it != list.end(); ++it )
	{
		std::cout << *it << std::endl;
	}

}