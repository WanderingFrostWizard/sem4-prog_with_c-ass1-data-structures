/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#     The Following Code has MODIFIED FROM intList tutorial which was       #
 *#                  originally written by Paul Miller                        #
 *#  custom_List.cpp - Custom linked list implementation based upon the       #
 *#             intList tutorial                                              #
 *#                                                                           #
 *###########################################################################*/

#include "custom_List.h"
using ll = Custom_List;

void ll::Node::set_next(std::unique_ptr<ll::Node>&& newnext)
{
    next = std::move(newnext);
}

ll::Node * ll::Node::get_next(void)
{
    return next.get();
}

std::unique_ptr<ll::Node>& ll::Node::get_next_ptr(void)
{
    return next;
}

std::string ll::Node::get_word(void)
{
    return word;
}

bool ll::add(std::string word)
{
    Node * current;
    Node * prev = nullptr;
    std::unique_ptr<ll::Node> newnode = std::make_unique<ll::Node>(word);
    if(head == nullptr)
    {
        head = std::make_unique<Node>(word);
        ++wordcount;
        return true;
    }
    current = head.get();//get the pointer held by the unique_ptr
    while(current && current->word < word)
    {
        prev = current;
        current = current->get_next();
    }
    if(!prev)
    {
        newnode->set_next(std::move(head));
        head = std::move(newnode);
    }
    else if(!current)
    {
        prev->set_next(std::move(newnode));
    }
    else
    {
        newnode->set_next(std::move(prev->get_next_ptr()));
        prev->set_next(std::move(newnode));
    }
    ++wordcount;
    return true;
}

// Searches for the first instance of the target word
bool ll::search(std::string targetWord)
{
    ll::Node * current;
    for(current = head.get(); current; current = current->get_next())
    {
        if ( current->word == targetWord )
            return true;
    }
    return false;
}

void ll::print(void)
{
    ll::Node * current;
    for(current = head.get(); current; current = current->get_next())
    {
        std::cout << current->word << std::endl;
    }
}
