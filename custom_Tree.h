/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  custom_Tree.h - My implementation of a binary tree                       #
 *#                                                                           #
 *###########################################################################*/

#include <memory>
#include <iostream>

class Custom_Tree
{

    class Node
    {
        std::string word;
        int timesRepeated;
        std::unique_ptr<Node> lChild;
        std::unique_ptr<Node> rChild;        

        public:
        Node(std::string newWord) : word(newWord), timesRepeated(1), 
                lChild(nullptr), rChild(nullptr) {}
        
        std::string get_word(void);
        // Get child nodes
        Custom_Tree::Node * getLeftChild(void);
        Custom_Tree::Node * getRightChild(void);

        void setLeftChild( std::unique_ptr<Custom_Tree::Node>&& Node );
        void setRightChild( std::unique_ptr<Custom_Tree::Node>&& Node );

        friend class Custom_Tree;
    };

    std::unique_ptr<Node>head;
    int uniqueWords;
    int wordCount;

    public:
    Custom_Tree(void) : head(nullptr), uniqueWords(0), wordCount(0) {}

    bool add(std::string newWord);
    bool search( std::string );
    void print( );
    void printNode( Node * );
};