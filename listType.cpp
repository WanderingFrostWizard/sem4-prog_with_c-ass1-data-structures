/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  list.cpp - Used to implement the std::list data structure                #
 *#                                                                           #
 *###########################################################################*/

#include "listType.h"


bool ListType::add(std::string word)
{
    list.push_back( word );
    ++wordcount;

    return true;
}

// Searches for the first instance of the target word
bool ListType::search(std::string targetWord)
{
    for ( std::list<std::string>::iterator it = list.begin(); it != list.end(); ++it )
    {
        if ( *it == targetWord )
            return true;        
    }
    return false;
}

void ListType::print(void)
{
    for ( std::list<std::string>::iterator it = list.begin(); it != list.end(); ++it )
    {
        std::cout << *it << std::endl;
    }
}