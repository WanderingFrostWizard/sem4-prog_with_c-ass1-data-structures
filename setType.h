/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  setType.h - Implementation of the std::multiset class                    #
 *#                                                                           #
 *###########################################################################*/

#ifndef SETTYPE_H
#define SETTYPE_H

#include <set>
#include <iostream>

class SetType

{
    std::multiset<std::string> list;

    int wordcount;
    public: 
        SetType( void )  {}
    bool add(std::string);
    bool search(std::string);
    void print(void);
};

#endif