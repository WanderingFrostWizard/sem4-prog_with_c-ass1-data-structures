/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  checker.cpp - Creates the data objects specifed in the main.cpp          #
 *#             Tokenizing using boost modified from example at               #
 *#      http://www.technical-recipes.com/2011/some-string-related-stuff/     #
 *#                                                                           #
 *###########################################################################*/

#include "checker.h"        // Include the relevant header file


// Generic template that can run each of the commands regardless of dataStructure
template <class Structure>
void loadDictionary ( Structure *dictionaryObject, std::string inputFile )
{
    std::cout << "Loading Dictionary" << std::endl;
    // String variable to store the current text 
    std::string text;

    boost::char_separator<char> sep(" 1234567890!@#$%^&*()_+=[{}]\\|;:'\"<>,./?");
    std::ifstream in( inputFile );

    while ( std::getline(in, text) )
    {
        std::transform(text.begin(), text.end(), text.begin(), ::tolower);
        boost::tokenizer< boost::char_separator< char > > tokens( text, sep );  
           
        BOOST_FOREACH( std::string val, tokens )  
        {  
            dictionaryObject->add( val );
        }
    }  
    // Close the file
    in.close();
}

template <class Structure>
void loadTextfile ( Structure *textfileObject, Structure *dictionaryObject, 
        std::string inputFile, Checker::wordcounter *counter )
{
    std::cout << "Loading Textfile" << std::endl;
    // String variable to store the current text 
    std::string text;
 
    boost::char_separator<char> sep(" 1234567890!@#$%^&*()_+=[{}]\\|;:'\"<>,./?"); 
    std::ifstream in( inputFile );

    while ( std::getline(in, text) )
    {
        std::transform(text.begin(), text.end(), text.begin(), ::tolower);
        boost::tokenizer< boost::char_separator< char > > tokens( text, sep );  
           
        BOOST_FOREACH( std::string val, tokens )  
        {  
            textfileObject->add( val );
            // Check if the word is in the dictionary and if it is, add it to 
            // the map of word counts
            if ( dictionaryObject->search( val ) )
                counter->add( val );
        }
    }  
    // Close the file
    in.close();
}


bool Checker::setStructure( std::string structure, std::string dictionaryName,
         std::string textfileName )
{
    // If structure IS a list compare returns 0 if it matches
    if (!structure.compare("list"))
    {
        std::cout << "Creating list Structures \n";
        ListType dictionaryObject;
        ListType textfileObject;

        loadDictionary<ListType>( &dictionaryObject, dictionaryName );
        loadTextfile<ListType>( &textfileObject, &dictionaryObject, 
                textfileName, &counter );
    }
    else if (!structure.compare("vector"))
    {
        std::cout << "Creating vector Structures \n";
        VectorType dictionaryObject;
        VectorType textfileObject;

        loadDictionary<VectorType>( &dictionaryObject, dictionaryName );
        loadTextfile<VectorType>( &textfileObject, &dictionaryObject, 
                textfileName, &counter );
        
    }
    else if (!structure.compare("set"))
    {
        std::cout << "Creating multi-set Structures \n";
        SetType dictionaryObject;
        SetType textfileObject;

        loadDictionary<SetType>( &dictionaryObject, dictionaryName );
        loadTextfile<SetType>( &textfileObject, &dictionaryObject, 
                textfileName, &counter );
    }
    else if (!structure.compare("custom_list"))
    {
        std::cout << "Creating custom list Structures \n";
        Custom_List dictionaryObject;
        Custom_List textfileObject;

        loadDictionary<Custom_List>( &dictionaryObject, dictionaryName );
        loadTextfile<Custom_List>( &textfileObject, &dictionaryObject, 
                textfileName, &counter );
    }
    else if (!structure.compare("custom_tree"))
    {
        std::cout << "Creating custom tree Structures \n";
        Custom_Tree dictionaryObject;
        Custom_Tree textfileObject;

        loadDictionary<Custom_Tree>( &dictionaryObject, dictionaryName );
        loadTextfile<Custom_Tree>( &textfileObject, &dictionaryObject, 
                textfileName, &counter );
    }
    else
    {
        std::cout << std::endl
        << "  Incorrect structure, available choices are \n"
        << "  list | dictionary | set | custom_list | custom_tree \n"
        << "  PROGRAM WILL NOW EXIT \n";
        return false;
    }
    return true;
}



bool Checker::wordcounter::add(std::string newWord)
{
    // Check if the word has already been inserted, if so increment its counter
    if( countMap.insert(std::make_pair( newWord, 1) ).second == false ) 
    {
        countMap[ newWord ] += 1;
    }
    return true;
}


// bool search(std::string);
void Checker::wordcounter::print(void)
{
    std::map<std::string, int>::iterator it = countMap.begin();
    while(it != countMap.end())
    {
        std::cout << it->first << ", " << it->second << std::endl;
        it++;
    }
}

void Checker::wordcounter::printToFile( std::string outputFile )
{
    std::ofstream out(outputFile);

    std::map<std::string, int>::iterator it = countMap.begin();
    while(it != countMap.end())
    {
        out << it->first << ", " << it->second << std::endl;
        it++;
    }

    out.close();
}
