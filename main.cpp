/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  main.cpp - Entry point into the program                                  #
 *#             Validates program arguments and passes them to checker        #
 *#                                                                           #
 *###########################################################################*/

#include <boost/program_options.hpp>    // To allow command line entry
#include <iostream> // For cin and cout
#include <string>   // For string functions
#include <fstream>  // To allow file handling

#include "checker.h"    // To allow access to the checking application

int main(int argc, const char *argv[])
{
    std::string structureType;  // Stores the state of the -s flag
    std::string dictionary; // Stores the name of the dictionary file 
    std::string textfile; // Stores the name of the text file 
    std::string outputfile; // Stores the name of the output file 

    boost::program_options::options_description desc{"Options"};
    desc.add_options()
        ("help,h", "Help screen")
        ("structure,s", boost::program_options::value(&structureType), "structure" )
        ("dictionary,d", boost::program_options::value(&dictionary), "dictionary" )
        ("textfile,t", boost::program_options::value(&textfile), "textfile" )
        ("outputfile,o", boost::program_options::value(&outputfile), "outputfile" )
    ;

    boost::program_options::variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    // Create an instance of the program, with the arguments from above
    Checker checkProgram;

    // Set the structure for dictionary and textfile if successfull, then output
    // the results to the output file ALSO Validates the structure argument
    if ( checkProgram.setStructure( structureType, dictionary, textfile ) )
    {
        // Print the word count map to the output file
        checkProgram.counter.printToFile( outputfile );
    }
  return EXIT_SUCCESS;
}

