/*#include <iostream>
//#include <boost/tokenizer.hpp>
#include <string>

#include <boost/program_options.hpp> 

//using namespace std;
 
int main ()
{

    std::string s = "a, b, c, d";
 
    boost::tokenizer<> tok(s);
 
 
    std::string s2 = "a|b|c|d";
 
 
    return 0;
}
*/


/*
// char_sep_example_1.cpp
#include <iostream>
#include <boost/tokenizer.hpp>
#include <string>

int main()
{
  std::string str = ";;Hello|world||-foo--bar;yow;baz|";
  typedef boost::tokenizer<boost::char_separator<char> > 
    tokenizer;
  boost::char_separator<char> sep("-;|");
  tokenizer tokens(str, sep);
  for (tokenizer::iterator tok_iter = tokens.begin();
       tok_iter != tokens.end(); ++tok_iter)
    std::cout << "<" << *tok_iter << "> ";
  std::cout << "\n";
  return EXIT_SUCCESS;
}



#include <iostream>
#include <boost/program_options.hpp> 
#include <string>

int main (int argc, const char *argv[] )
{

}

*/




/*

#include <boost/program_options.hpp>
#include <iostream>

using namespace boost::program_options;

void on_age(int age)
{
  std::cout << "On age: " << age << '\n';
}

int main(int argc, const char *argv[])
{
  try
  {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("pi", value<float>()->default_value(3.14f), "Pi")
      ("age", value<int>()->notifier(on_age), "Age");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
      std::cout << desc << '\n';
    else if (vm.count("age"))
      std::cout << "Age: " << vm["age"].as<int>() << '\n';
    else if (vm.count("pi"))
      std::cout << "Pi: " << vm["pi"].as<float>() << '\n';
  }
  catch (const error &ex)
  {
    std::cerr << ex.what() << '\n';
  }
}*/




// #include <boost/program_options.hpp> 
 
// #include <iostream> 
// #include <string> 
 
//  namespace po = boost::program_options;
 
 
// int main(int argc, char** argv) 
// { 

//   // Declare the supported options.
//   po::options_description desc("Allowed options");
//   desc.add_options()
//       ("help", "produce help message")
//       ("compression", po::value<int>(), "set compression level")
//   ;

//   po::variables_map vm;
//   po::store(po::parse_command_line(ac, av, desc), vm);
//   po::notify(vm);    

//   if (vm.count("help")) {
//       cout << desc << "\n";
//       return 1;
//   }

//   if (vm.count("compression")) {
//       cout << "Compression level was set to " 
//    << vm["compression"].as<int>() << ".\n";
//   } else {
//       cout << "Compression level was not set.\n";
//   }
 
//   return SUCCESS; 
 
// } // main 










#include <boost/program_options.hpp>
#include <iostream>

using namespace boost::program_options;

void on_age(int age)
{
  std::cout << "On age: " << age << '\n';
}

int main(int argc, const char *argv[])
{
  // try
  // {
    options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("pi", value<float>()->default_value(3.14f), "Pi")
      ("age", value<int>()->notifier(on_age), "Age");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
      std::cout << desc << '\n';
    else if (vm.count("age"))
      std::cout << "Age: " << vm["age"].as<int>() << '\n';
    else if (vm.count("pi"))
      std::cout << "Pi: " << vm["pi"].as<float>() << '\n';
  // }
  // catch (const error &ex)
  // {
  //   std::cerr << ex.what() << '\n';
  // }
}