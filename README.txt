/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                 Readme FILE                               #
 *#                                                                           #
 *###########################################################################*/

File structure


    Main.cpp

        |
        v

    Checker.cpp   ->    Checker.h

        |
        v
    ( LINKS TO EACH OF THE FOLLOWING )

    setType.cpp   ->    setType.h
    listType.cpp  ->    listType.h
    vectorType.cpp ->   vectorType.h
    customList.cpp ->   customList.h
    customTree.cpp ->   customTree.h    ( NOT COMPLETELY IMPLEMENTED )


Use "make script" on the RMIT servers to execute the 
    "source /opt/rh/devtoolset-6/enable" command

I have yet to finish the 

# Edit Distance
# Binary Search tree Balancing
# Validation throughout program
# Error checking eg try-catch blocks
# File checking eg open / close success/failure
