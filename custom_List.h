/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#     The Following Code has MODIFIED FROM intList tutorial which was       #
 *#                  originally written by Paul Miller                        #
 *#  custom_List.h - Custom linked list implementation based upon the         #
 *#             intList tutorial                                              #
 *#                                                                           #
 *###########################################################################*/

#ifndef CUSTOM_LIST_H
#define CUSTOM_LIST_H

#include <memory>
#include <iostream>

class Custom_List
{
    class Node
    {
        std::string word;
        std::unique_ptr<Node> next;
        public:
        Node(std::string newdata) : word(newdata), next(nullptr) {}

        void set_next(std::unique_ptr<Node>&& newnext);
        Node * get_next(void);
        std::string get_word(void);
        std::unique_ptr<Node>& get_next_ptr(void);
        friend class Custom_List;
    };

    std::unique_ptr<Node>head;
    int wordcount;
    public:
    Custom_List(void) : head(nullptr), wordcount(0) {}
    bool add(std::string);
    bool search(std::string);
    void print(void);
};
#endif