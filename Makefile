#############################################################################
#                                                                           #
#                     C++ Assignment 1 - Data Structures                    #
#                     Written by Cameron Watt - s3589163                    #
#                                 August 2017                               #
#                                                                           #
#  Makefile - Makefile to compile assignment 1                              #
#                                                                           #
#############################################################################

CC=g++

LFLAGS=  -lboost_system -lboost_program_options
CFLAGS= -Wall -pedantic -std=c++14 -g

OBJECTS := setType.o listType.o vectorType.o custom_List.o custom_Tree.o checker.o main.o
BIN := ass1

all:$(BIN)

# Link Objects together
$(BIN):$(OBJECTS)
		$(CC) $(OBJECTS) -o ass1 $(LFLAGS)

# Make Object files
listType.o: listType.cpp listType.h
		$(CC) -c listType.cpp $(CFLAGS)

vectorType.o: vectorType.cpp vectorType.h
		$(CC) -c vectorType.cpp $(CFLAGS)

setType.o: setType.cpp setType.h
		$(CC) -c setType.cpp $(CFLAGS)

custom_List.o: custom_List.cpp custom_List.h
		$(CC) -c custom_List.cpp $(CFLAGS)

custom_Tree.o: custom_Tree.cpp custom_Tree.h
		$(CC) -c custom_Tree.cpp $(CFLAGS)

checker.o: checker.cpp checker.h
		$(CC) -c checker.cpp $(CFLAGS)

main.o: main.cpp checker.h custom_List.h
		$(CC) -c main.cpp $(CFLAGS)

# Remove objects and binaries
clean:
		rm  $(OBJECTS) $(BIN)

# Runs the script needed for RMIT servers
script:
	source /opt/rh/devtoolset-6/enable
