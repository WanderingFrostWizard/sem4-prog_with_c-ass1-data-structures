/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  custom_Tree.cpp - My implementation of a binary tree                     #
 *#                                                                           #
 *###########################################################################*/

#include "custom_Tree.h"

std::string Custom_Tree::Node::get_word(void)
{
    return word;
}

Custom_Tree::Node * Custom_Tree::Node::getLeftChild(void)
{
    return lChild.get();
}

Custom_Tree::Node * Custom_Tree::Node::getRightChild(void)
{
    return rChild.get();
}

void Custom_Tree::Node::setLeftChild( std::unique_ptr<Custom_Tree::Node>&& Node )
{
    lChild = std::move(Node);
}

void Custom_Tree::Node::setRightChild( std::unique_ptr<Custom_Tree::Node>&& Node )
{
    rChild = std::move(Node);
}


bool Custom_Tree::add(std::string newWord)
{
    bool movedLeft = false; // Used to track movements through the tree
    Node * current;
    Node * prev;
    std::unique_ptr<Custom_Tree::Node> newnode = std::make_unique<Custom_Tree::Node>(newWord);

    // Check if the head is null IE empty tree
    if ( head == nullptr )
    {
        head = std::make_unique<Node>(newWord);
        uniqueWords = 1;
        wordCount = 1;
        return true;
    }

    current = head.get();
    // Function needs to continue to the bottom of the tree
    while ( 1 ) 
    {
        // If the current Node cointains the newWord
        // Increase the total wordCount but NOT the number of unique words
        if ( current->get_word() == newWord )
        {
            current->timesRepeated += 1;
            wordCount += 1;
            return true;
        }

        // Set the previous Node
        prev = current;
        // Move to the next Node
        if ( current->get_word() > newWord )
        {
            current = current->getLeftChild();
            movedLeft = true;
        }
        else 
        { 
            current = current->getRightChild();
            movedLeft = false;
        }

        // If the current Node is empty
        if ( current == nullptr )
        {
            if ( movedLeft )
                prev->setLeftChild(std::move(newnode));
            else 
                prev->setRightChild(std::move(newnode));
            uniqueWords += 1;
            wordCount += 1;
            return true;
        }
    }
}


bool Custom_Tree::search( std::string targetWord )
{
    Node * current = head.get();

    // Check if the head is null
    if ( current == nullptr )
        return false;

    while ( current != nullptr ) 
    {
        // Check current node's word
        if ( current->get_word() == targetWord )
            return true;

        // CHECK WE NEED TO MOVE LEFT OR RIGHT
        if ( current->get_word() > targetWord )
        {
            // Check if the pointer is valid before checking it
            if ( current->lChild == nullptr )
                return false;
            // If the current node's child is valid, move to it
            else 
                current = current->getLeftChild();
        }
        else 
        {
            // Check if the pointer is valid before checking it
            if ( current->rChild == nullptr )
                return false;
            // If the current node's child is valid, move to it
            else 
                current = current->getRightChild();
        }
    }

    return false;
}

void Custom_Tree::print( )
{
    // Custom_Tree::printNode( std::get(head) );
    Custom_Tree::printNode( head.get() );
}

void Custom_Tree::printNode( Node * current )
{
    if ( current->lChild != nullptr )
        Custom_Tree::printNode( current->lChild.get() );

    if ( current->rChild != nullptr )
        Custom_Tree::printNode( current->rChild.get() );

    std::cout << current->get_word() << std::endl;
}