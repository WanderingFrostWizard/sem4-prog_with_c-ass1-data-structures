/*#############################################################################
 *#                                                                           #
 *#                     C++ Assignment 1 - Data Structures                    #
 *#                     Written by Cameron Watt - s3589163                    #
 *#                                 August 2017                               #
 *#                                                                           #
 *#  vectorType.h - Implementation of the std::vector class                   #
 *#                                                                           #
 *###########################################################################*/

#ifndef VECTORTYPE_H
#define VECTORTYPE_H

#include <vector>
#include <iostream>

class VectorType
{
	std::vector<std::string> list;

	int wordcount;
	public: 
		VectorType( void )  {}
	bool add(std::string);
    bool search(std::string);
    void print(void);
};

#endif